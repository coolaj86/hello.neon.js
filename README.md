# hello

I wanted to try out neon (rust bindings for node.js),
so I started reading [the neon guide](https://neon-bindings.com/docs/intro)
and spun this up as my playground.

There's probably not too much interesting here,
but if you look in `native/src/lib.rs` you'll see
the examples I've been trying out.

```bash
npm install -g neon
```

```bash
git clone https://git.coolaj86.com/coolaj86/hello.neon.js.git
pushd hello.neon.js/
neon build --release
node
```

```js
var hello = require('.');
console.log(hello);

hello.any();
```

```bash
node example.js
```

## How I made this

1. Install [Node.js](https://nodejs.org).
2. Install build tools
  * macOS: XCode and `xcode-select install` (CLI Tools)
  * Windows: `npm install --global --production windows-build-tools`
  * Ubuntu: `sudo apt install -y build-essential`
3. Install Rust
4. Install neon; Create (and rename) project

```bash
npm install --global neon-cli
neon version
neon new hello.neon
mv hello.neon hello.neon.js
pushd hello.neon.js
neon build
```

Then I just followed along in
[the docs](https://neon-bindings.com/docs/hello-world)
and [the examples](https://github.com/neon-bindings/examples)
