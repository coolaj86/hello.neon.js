'use strict';

var hello = require('.');
//var hello = require('hello.neon');

console.log('hello()', hello.hello());
console.log('threadCount()', hello.threadCount());
console.log('greet("Bob")', hello.greet("Bob"));
console.log('noop()', hello.noop());
console.log('any()', hello.any());
console.log('any()', hello.any());
console.log('any()', hello.any());
console.log('any()', hello.any());
console.log('any()', hello.any());
console.log('any()', hello.any());
console.log('any()', hello.any());

if (Math.round(Math.random() * 1)) {
  console.log('throw()');
  console.log(hello.throw());
} else {
  try {
    console.log(console.log('panic()'));
    console.log(hello.panic());
  } catch(e) {
    console.error('caught error:');
    console.error(e);
  }
}

console.log("all done");
